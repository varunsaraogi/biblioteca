package controller;

import model.ItemType;
import model.Library;
import model.Result;
import view.LibraryView;

public class LibraryController {

    private LibraryView libraryView;
    private Library library;

    public LibraryController(Library library, LibraryView libraryView) {

        this.libraryView = libraryView;
        this.library = library;
    }

    public void listItems(ItemType itemType) {
        libraryView.listItems(library.getItemsDetails(itemType));
    }


    public void initializeCheckoutFor(ItemType itemType) {
        listItems(itemType);
        String option = libraryView.getOptionFor(itemType);
        Result result = library.checkoutItem(itemType, option);
        if (result.shouldProceed()) {
            libraryView.notifyCheckoutFor(itemType);
            return;
        }
        libraryView.notifyInvalidCheckoutFor(itemType);
    }

    public void initializeReturnFor(ItemType itemType) {
        String option = libraryView.getOptionFor(itemType);
        Result result = library.returnItem(itemType, option);
        if (result.shouldProceed()) {
            libraryView.notifyReturnFor(itemType);
            return;
        }
        libraryView.notifyInvalidReturnFor(itemType);
    }
}
