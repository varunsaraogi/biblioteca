package controller;

import dependency.Dependencies;
import model.CommandFactory;
import model.Result;
import model.command.Command;
import view.ApplicationView;

public class ApplicationController {//controls application related operations
    private ApplicationView applicationView;
    private MenuController menuController;
    private CommandFactory commandFactory;
    private AccessController accessController;

    public ApplicationController(Dependencies dependencies) {
        applicationView = (ApplicationView) dependencies.getDependencyFor(ApplicationView.class);
        menuController = (MenuController) dependencies.getDependencyFor(MenuController.class);
        commandFactory = (CommandFactory) dependencies.getDependencyFor(CommandFactory.class);
        accessController = (AccessController) dependencies.getDependencyFor(AccessController.class);
    }

    public void startApplication() {
        applicationView.welcomeUser();

        Command command;
        Result result = new Result(true);
        do {
            menuController.displayMenu();
            String userChoice = menuController.acceptUserOption();
            command = commandFactory.getCommandFor(userChoice);
            if (command.requiresAuthentication()) {
                boolean loggedIn = accessController.initializeAuthentication();
                if (!loggedIn) {
                    continue;
                }
            }
            result = command.execute();
        } while (result.shouldProceed());
        applicationView.notifyTermination();
    }
}

