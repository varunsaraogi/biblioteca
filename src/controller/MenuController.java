package controller;

import model.Menu;
import view.MenuView;

public class MenuController {//controls menu related operations
    private Menu menu;
    private MenuView menuView;

    public MenuController(Menu menu, MenuView menuView) {
        this.menu = menu;
        this.menuView = menuView;
    }

    public void displayMenu() {
        menuView.list(menu.getOptions());
    }

    public String acceptUserOption() {
        return menuView.fetchOption();
    }

    public void handleInvalidOption() {
        menuView.notifyInvalidOption();
    }

    public void userHasLoggedIn() {
        menu.showLogoutOptions();
    }

    public void userHasLoggedOut() {
        menu.hideLogoutOptions();
    }
}
