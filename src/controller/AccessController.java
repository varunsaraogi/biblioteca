package controller;

import model.Access;
import view.AccessView;

public class AccessController {
    private Access access;
    private AccessView accessView;
    private MenuController menuController;

    public AccessController(Access access, AccessView accessView, MenuController menuController) {
        this.access = access;
        this.accessView = accessView;
        this.menuController = menuController;
    }

    private boolean isAuthenticated() {

        boolean loggedIn = access.isLoggedIn();
        if (!loggedIn)
            accessView.notifyUnknownUser();
        return loggedIn;
    }

    public boolean initializeAuthentication() {
        if (isAuthenticated())
            return true;
        String username = accessView.getUsername();
        if (access.verifyUsername(username)) {
            String password = accessView.getPassword();
            if (access.verifyCredential(username, password)) {
                accessView.notifySuccessfulLogin();
                menuController.userHasLoggedIn();
                return true;
            }
        }
        accessView.notifyInvalidCredential();
        return false;
    }

    public void initializeLogout() {
        access.logoutUser();
        menuController.userHasLoggedOut();
    }

    public void showUserInformation() {
        accessView.listUserInformation(access.getUserInformation());
    }
}
