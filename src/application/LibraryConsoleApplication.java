package application;

import controller.ApplicationController;
import dependency.Dependencies;

public class LibraryConsoleApplication {//injects dependencies and starts application

    public static void main(String[] args) {

        Dependencies dependencies = Dependencies.initialize();
        ApplicationController applicationController = new ApplicationController(dependencies);
        applicationController.startApplication();
    }
}