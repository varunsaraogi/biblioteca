package dependency;

import console.SystemConsole;
import controller.LibraryController;
import controller.AccessController;
import controller.MenuController;
import model.*;
import model.command.*;
import model.item.Book;
import model.item.Movie;
import view.AccessView;
import view.ApplicationView;
import view.LibraryView;
import view.MenuView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Dependencies {
    private final HashMap<Class<?>, Object> dependencies;

    public Dependencies() {
        dependencies = new HashMap<>();
    }

    public Object getDependencyFor(Class dependencyClass) {
        return dependencies.get(dependencyClass);
    }

    public void register(Class dependencyClass, Object dependency) {
        dependencies.put(dependencyClass, dependency);
    }

    public static Dependencies initialize() {


        SystemConsole systemConsole = new SystemConsole(System.in, System.out);
        ApplicationView applicationView = new ApplicationView(systemConsole);

        List<String> options = getPreExistingOptions();
        List<String> logoutOptions = new ArrayList<>();
        logoutOptions.add("l. Logout");
        logoutOptions.add("u. User Information");
        Menu menu = new Menu(options, logoutOptions);
        MenuView menuView = new MenuView(systemConsole);
        MenuController menuController = new MenuController(menu, menuView);

        Library library = getPreExistingLibrary();
        LibraryView libraryView = new LibraryView(systemConsole);
        LibraryController libraryController = new LibraryController(library, libraryView);

        Access access = getPreExistingUserCredentials();
        AccessView accessView = new AccessView(systemConsole);
        AccessController accessController = new AccessController(access, accessView, menuController);

        Map<String, Command> commands = getPreExistingCommandsFor(libraryController, accessController);
        InvalidOptionCommand invalidOptionCommand = new InvalidOptionCommand(menuController);
        CommandFactory commandFactory = new CommandFactory(commands, invalidOptionCommand);

        Dependencies dependencies = new Dependencies();

        dependencies.register(ApplicationView.class, applicationView);

        dependencies.register(MenuController.class, menuController);

        dependencies.register(CommandFactory.class, commandFactory);

        dependencies.register(AccessController.class, accessController);

        return dependencies;
    }

    private static Access getPreExistingUserCredentials() {
        Access access = new Access();
        access.addUserCredential("111-1111", "111", new User("Varun Saraogi", "svarun@thoughtworks.com", "9040398926"));
        access.addUserCredential("222-2222", "222", new User("Arun Gupta", "agupta@thoughtworks.com", "8116355345"));
        return access;
    }


    private static Map<String, Command> getPreExistingCommandsFor(LibraryController libraryController, AccessController accessController) {
        Map<String, Command> commands = new HashMap<>();
        commands.put("1", new ListItemsCommand(libraryController, ItemType.BOOK));
        commands.put("2", new CheckoutItemCommand(libraryController, ItemType.BOOK));
        commands.put("3", new ReturnItemCommand(libraryController, ItemType.BOOK));
        commands.put("4", new ListItemsCommand(libraryController, ItemType.MOVIE));
        commands.put("5", new CheckoutItemCommand(libraryController, ItemType.MOVIE));
        commands.put("6", new ReturnItemCommand(libraryController, ItemType.MOVIE));
        commands.put("q", new QuitApplicationCommand());
        commands.put("l", new LogoutCommand(accessController));
        commands.put("u", new ShowUserInformationCommand(accessController));
        return commands;
    }

    private static List<String> getPreExistingOptions() {
        List<String> options = new ArrayList<>();
        options.add("1. List Books");
        options.add("2. Checkout a Book");
        options.add("3. Return a Book");
        options.add("4. List Movies");
        options.add("5. Checkout a Movie");
        options.add("6. Return a Movie");
        options.add("q. Quit");
        return options;
    }

    private static Library getPreExistingLibrary() {
        Items books = new Items(ItemType.BOOK);
        books.add(new Book("A Walk To Remember", "Nicholas Sparks", 1999));
        books.add(new Book("Five Point Someone", "Chetan Bhagat", 2004));
        List<Items> itemsList = new ArrayList<>();
        itemsList.add(books);
        Items movies = new Items(ItemType.MOVIE);
        movies.add(new Movie("Inception", 2010, "Christopher Nolan", "10"));
        movies.add(new Movie("Inception", 2014, "Christopher Nolan"));
        itemsList.add(movies);
        return new Library(itemsList);
    }
}
