package view;

import console.SystemConsole;

import java.util.List;

public class MenuView {//manages user interaction with menu
    private SystemConsole systemConsole;

    public MenuView(SystemConsole systemConsole) {
        this.systemConsole = systemConsole;
    }

    public void list(List<String> options) {
        systemConsole.display("\n\n=========================Biblioteca Main Menu=========================");
        for (String option : options)
            systemConsole.display(option);
    }

    public String fetchOption() {
        systemConsole.displayToGetChoice("\nEnter option : ");
        return systemConsole.get();
    }

    public void notifyInvalidOption() {
        systemConsole.display("\nYou entered an invalid option...");
    }

}
