package view;

import console.SystemConsole;

public class ApplicationView {//manages user interaction with application
    private SystemConsole systemConsole;

    public ApplicationView(SystemConsole systemConsole) {

        this.systemConsole = systemConsole;
    }

    public void welcomeUser() {
        systemConsole.display("Welcome to Biblioteca");
    }

    public void notifyTermination() {
        systemConsole.display("Exiting Biblioteca...");
    }
}
