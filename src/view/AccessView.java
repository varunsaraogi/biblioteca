package view;

import console.SystemConsole;

public class AccessView {

    private SystemConsole systemConsole;

    public AccessView(SystemConsole systemConsole) {
        this.systemConsole = systemConsole;
    }

    public String getUsername() {
        systemConsole.displayToGetChoice("Enter Username : ");
        return systemConsole.get();
    }

    public String getPassword() {
        systemConsole.displayToGetChoice("Enter Password : ");
        return systemConsole.get();
    }

    public void notifyUnknownUser() {
        systemConsole.display("You have to login first...");
    }

    public void notifySuccessfulLogin() {
        systemConsole.display("You have successfully logged in...");
    }

    public void notifyInvalidCredential() {
        systemConsole.display("You entered invalid credential...");
    }

    public void listUserInformation(String userInformation) {
        systemConsole.display(userInformation);
    }
}
