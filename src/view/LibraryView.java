package view;

import console.SystemConsole;
import model.ItemType;

import java.util.List;

public class LibraryView {//manages user interaction with library
    private SystemConsole systemConsole;

    public LibraryView(SystemConsole systemConsole) {

        this.systemConsole = systemConsole;
    }

    public void listItems(List<String> items) {
        for (String item : items)
            systemConsole.display(item);
    }

    public void notifyCheckoutFor(ItemType itemType) {
        systemConsole.display("Thank you! Enjoy the " + itemType.getName().toLowerCase() + ".");
    }

    public void notifyReturnFor(ItemType itemType) {
        systemConsole.display("Thank you for returning the " + itemType.getName().toLowerCase() + ".");
    }

    public void notifyInvalidCheckoutFor(ItemType itemType) {
        systemConsole.display("That " + itemType.getName().toLowerCase() + " is not available.");
    }

    public String getOptionFor(ItemType itemType) {

        systemConsole.displayToGetChoice("Enter " + itemType.getName() + " ID : ");
        return systemConsole.get();
    }

    public void notifyInvalidReturnFor(ItemType itemType) {
        systemConsole.display("That is not a valid " + itemType.getName().toLowerCase() + " to return.");
    }
}
