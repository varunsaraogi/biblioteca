package console;

import java.io.InputStream;
import java.io.PrintStream;
import java.util.Scanner;

public class SystemConsole {
    private InputStream inStream;
    private PrintStream outStream;

    public SystemConsole(InputStream inStream, PrintStream outStream) {
        this.inStream = inStream;
        this.outStream = outStream;
    }

    public void display(String message) {
        outStream.println(message);
    }

    public String get() {
        Scanner scanner = new Scanner(inStream);
        return scanner.nextLine();
    }

    public void displayToGetChoice(String message) {
        outStream.print(message);
    }

}
