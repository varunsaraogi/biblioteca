package model;

import model.command.Command;
import model.command.InvalidOptionCommand;

import java.util.Map;

public class CommandFactory {

    private Map<String, Command> commands;
    private InvalidOptionCommand invalidOptionCommand;

    public CommandFactory(Map<String, Command> commands, InvalidOptionCommand invalidOptionCommand) {
        this.invalidOptionCommand = invalidOptionCommand;
        this.commands = commands;
    }

    public Command getCommandFor(String option) {
        if (commands.containsKey(option))
            return commands.get(option);
        return invalidOptionCommand;
    }
}
