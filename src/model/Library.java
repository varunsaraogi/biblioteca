package model;

import java.util.List;

public class Library {
    List<Items> itemsList;

    public Library(List<Items> itemsList) {
        this.itemsList = itemsList;
    }

    public List<String> getItemsDetails(ItemType itemType) {
        return findItemsFor(itemType).getItems();
    }

    private Items findItemsFor(ItemType itemType) {
        return itemsList.stream().filter(items -> items.getItemType() == itemType).findAny().get();
    }

    public Result checkoutItem(ItemType itemType, String option) {
        int itemIndex = Integer.parseInt(option) - 1;
        Items items = findItemsFor(itemType);
        if (items.validateItemCheckout(itemIndex)) {
            items.checkoutItem(itemIndex);
            return new Result(true);
        }
        return new Result(false);
    }

    public Result returnItem(ItemType itemType, String option) {
        int itemIndex = Integer.parseInt(option) - 1;
        Items items = findItemsFor(itemType);
        if (items.validateItemReturn(itemIndex)) {
            items.returnItem(itemIndex);
            return new Result(true);
        }
        return new Result(false);
    }
}
