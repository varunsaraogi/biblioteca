package model;

public class Result {
    private final boolean shouldProceed;

    public Result(boolean shouldProceed) {

        this.shouldProceed = shouldProceed;
    }
    public boolean shouldProceed() {
        return shouldProceed;
    }
}
