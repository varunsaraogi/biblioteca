package model;

public class User {
    private final String name;
    private final String email;
    private final String mobile;

    public User(String name, String email, String mobile) {

        this.name = name;
        this.email = email;
        this.mobile = mobile;
    }

    public String getInformation() {
        return "Name : " + name + "\tEmail : " + email + "\tMobile : " + mobile;
    }
}
