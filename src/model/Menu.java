package model;

import java.util.ArrayList;
import java.util.List;

public class Menu {
    private List<String> options;
    private List<String> logoutOptions;
    private boolean shouldDisplayLogoutOptions;

    public Menu(List<String> options, List<String> logoutOptions) {
        this.options = options;
        this.logoutOptions = logoutOptions;
        shouldDisplayLogoutOptions = false;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Menu menu = (Menu) o;

        return options != null ? options.equals(menu.options) : menu.options == null;

    }

    @Override
    public int hashCode() {
        return options != null ? options.hashCode() : 0;
    }

    public List<String> getOptions() {
        if (shouldDisplayLogoutOptions) {
            List<String> allOptions = new ArrayList<>();
            allOptions.addAll(options);
            allOptions.addAll(logoutOptions);
            return allOptions;
        }
        return options;
    }

    public void showLogoutOptions() {
        shouldDisplayLogoutOptions = true;
    }

    public void hideLogoutOptions() {
        shouldDisplayLogoutOptions = false;
    }
}
