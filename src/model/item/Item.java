package model.item;

public abstract class Item {

    private boolean isIssued;

    public Item() {
        isIssued = false;
    }

    public boolean isCheckedOut() {
        return isIssued;
    }

    public void updateCheckout() {
        isIssued = true;
    }

    public void updateReturn() {
        isIssued = false;
    }

    public abstract String getDetails();

}
