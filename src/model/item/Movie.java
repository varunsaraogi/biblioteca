package model.item;

public class Movie extends Item {

    private final String name;
    private final int year;
    private final String director;
    private final String rating;

    public Movie(String name, int year, String director, String rating) {
        super();
        this.name = name;
        this.year = year;
        this.director = director;
        this.rating = new String(rating);
    }

    public Movie(String name, int year, String director) {
        this(name, year, director, "unrated");
    }

    @Override
    public String getDetails() {
        return name + "\t" + year + "\t" + director + "\t" + rating;
    }
}
