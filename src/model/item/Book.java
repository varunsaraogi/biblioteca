package model.item;

public class Book extends Item {//manages book details

    private String name;
    private final String authorName;
    private final int yearPublished;

    public Book(String name, String authorName, int yearPublished) {
        super();
        this.name = name;
        this.authorName = authorName;
        this.yearPublished = yearPublished;
    }

    @Override
    public String getDetails() {
        return name + "\t" + authorName + "\t" + yearPublished;
    }
}
