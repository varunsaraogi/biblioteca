package model.command;

import controller.LibraryController;
import model.ItemType;
import model.Result;

public class CheckoutItemCommand implements Command {
    private LibraryController libraryController;
    private ItemType itemType;

    public CheckoutItemCommand(LibraryController libraryController, ItemType itemType) {
        this.libraryController = libraryController;
        this.itemType = itemType;
    }

    @Override
    public boolean requiresAuthentication() {
        return true;
    }

    @Override
    public Result execute() {
        libraryController.initializeCheckoutFor(itemType);
        return new Result(true);
    }
}
