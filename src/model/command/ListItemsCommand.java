package model.command;

import controller.LibraryController;
import model.ItemType;
import model.Result;

public class ListItemsCommand implements Command {
    private LibraryController libraryController;
    private ItemType itemType;

    public ListItemsCommand(LibraryController libraryController, ItemType itemType) {
        this.libraryController = libraryController;
        this.itemType = itemType;
    }

    @Override
    public boolean requiresAuthentication() {
        return false;
    }

    public Result execute() {
        libraryController.listItems(itemType);
        return new Result(true);
    }
}
