package model.command;

import controller.AccessController;
import controller.MenuController;
import model.Result;

public class LogoutCommand implements Command {
    private AccessController accessController;

    public LogoutCommand(AccessController accessController) {
        this.accessController = accessController;
    }

    @Override
    public boolean requiresAuthentication() {
        return true;
    }

    @Override
    public Result execute() {
        accessController.initializeLogout();
        return new Result(true);
    }
}
