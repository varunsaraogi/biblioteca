package model.command;

import controller.MenuController;
import model.Result;

public class InvalidOptionCommand implements Command {
    private MenuController menuController;

    public InvalidOptionCommand(MenuController menuController) {
        this.menuController = menuController;
    }

    @Override
    public boolean requiresAuthentication() {
        return false;
    }

    public Result execute() {
        menuController.handleInvalidOption();
        return new Result(true);
    }
}
