package model.command;

import model.Result;

public interface Command {
    boolean requiresAuthentication();
    Result execute();
}
