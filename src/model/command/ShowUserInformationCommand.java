package model.command;

import controller.AccessController;
import model.Result;

public class ShowUserInformationCommand implements Command {
    private AccessController accessController;

    public ShowUserInformationCommand(AccessController accessController) {
        this.accessController = accessController;
    }

    @Override
    public boolean requiresAuthentication() {
        return true;
    }

    @Override
    public Result execute() {
        accessController.showUserInformation();
        return new Result(true);
    }
}
