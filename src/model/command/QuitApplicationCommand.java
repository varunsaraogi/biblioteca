package model.command;

import model.Result;

public class QuitApplicationCommand implements Command {

    @Override
    public boolean requiresAuthentication() {
        return false;
    }

    public Result execute() {
        return new Result(false);
    }
}
