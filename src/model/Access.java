package model;

import java.util.HashMap;
import java.util.Map;

public class Access {
    private String loggedInUserId;
    private Map<String, String> credentials;
    private Map<String, User> users;

    public Access() {
        loggedInUserId = "";
        credentials = new HashMap<>();
        users = new HashMap<>();
    }

    public boolean isLoggedIn() {
        if (loggedInUserId.equals(""))
            return false;
        return true;
    }

    public void addUserCredential(String username, String password, User user) {
        credentials.put(username, password);
        users.put(username, user);
    }

    public boolean verifyUsername(String username) {
        if (credentials.containsKey(username))
            return true;
        return false;
    }

    public boolean verifyCredential(String username, String password) {
        if (password.equals(credentials.get(username))) {
            loggedInUserId = username;
            return true;
        }
        return false;
    }

    public void logoutUser() {
        loggedInUserId = "";
    }

    public String getUserInformation() {
        return users.get(loggedInUserId).getInformation();
    }
}
