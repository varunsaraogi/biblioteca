package model;

public enum ItemType {
    BOOK("Book"),MOVIE("Movie");

    private String name;

    ItemType(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}