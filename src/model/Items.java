package model;

import model.item.Item;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Items {
    private List<Item> items;
    private ItemType itemType;

    public ItemType getItemType() {
        return itemType;
    }


    public Items(ItemType itemType) {
        this.itemType = itemType;
        items = new ArrayList<>();
    }


    public void add(Item item) {
        items.add(item);
    }


    public List<String> getItems() {

        return items.stream().filter(itum -> !itum.isCheckedOut()).
                map(item -> ((items.indexOf(item) + 1) + ". " + item.getDetails())).
                collect(Collectors.toList());
    }


    private boolean validateItemIndex(int itemIndex) {
        return 0 <= itemIndex && itemIndex < items.size();
    }

    public void checkoutItem(int itemIndex) {
        Item item = items.get(itemIndex);
        item.updateCheckout();
    }

    public void returnItem(int itemIndex) {
        items.get(itemIndex).updateReturn();
    }

    public boolean validateItemCheckout(int itemIndex) {
        return validateItemIndex(itemIndex) && !items.get(itemIndex).isCheckedOut();
    }

    public boolean validateItemReturn(int itemIndex) {
        return validateItemIndex(itemIndex) && items.get(itemIndex).isCheckedOut();
    }

}
