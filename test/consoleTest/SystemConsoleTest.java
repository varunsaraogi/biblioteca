package consoleTest;

import console.SystemConsole;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.junit.Assert.assertEquals;

public class SystemConsoleTest {
    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private final ByteArrayInputStream inContent = new ByteArrayInputStream("Message".getBytes());

    @Before
    public void setUpStreams() {

        System.setOut(new PrintStream(outContent));
        System.setIn(inContent);
    }

    @After
    public void cleanUpStreams() {
        System.setOut(null);
        System.setIn(null);
    }

    @Test
    public void shouldDisplayMessage() {
        SystemConsole systemConsole = new SystemConsole(System.in, System.out);
        systemConsole.display("Message");

        String actualMessage = outContent.toString();

        assertEquals("Message\n", actualMessage);
    }

    @Test
    public void shouldAcceptString() {
        SystemConsole systemConsole = new SystemConsole(System.in, System.out);

        String message = systemConsole.get();

        assertEquals("Message", message);
    }


    @Test
    public void shouldDisplayMessageWithoutGoingToNextLine() {
        SystemConsole systemConsole = new SystemConsole(System.in, System.out);
        systemConsole.displayToGetChoice("Message");

        String message = outContent.toString();

        assertEquals("Message", message);
    }
}
