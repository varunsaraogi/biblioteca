package viewTest;

import console.SystemConsole;
import org.junit.Test;
import org.mockito.InOrder;
import view.AccessView;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

public class AccessViewTest {
    @Test
    public void shouldReturnUsernameWithProperMessage() {
        SystemConsole mockedSystemConsole = mock(SystemConsole.class);
        AccessView accessView = new AccessView(mockedSystemConsole);
        when(mockedSystemConsole.get()).thenReturn("111-1111");
        String expectedUsername = "111-1111";

        String actualUsername = accessView.getUsername();

        InOrder inOrder = inOrder(mockedSystemConsole);
        inOrder.verify(mockedSystemConsole).displayToGetChoice("Enter Username : ");
        inOrder.verify(mockedSystemConsole).get();
        assertEquals(expectedUsername, actualUsername);
    }

    @Test
    public void shouldReturnPasswordWithProperMessage() {
        SystemConsole mockedSystemConsole = mock(SystemConsole.class);
        AccessView accessView = new AccessView(mockedSystemConsole);
        when(mockedSystemConsole.get()).thenReturn("111");
        String expectedPassword = "111";

        String actualPassword = accessView.getPassword();

        InOrder inOrder = inOrder(mockedSystemConsole);
        inOrder.verify(mockedSystemConsole).displayToGetChoice("Enter Password : ");
        inOrder.verify(mockedSystemConsole).get();
        assertEquals(expectedPassword, actualPassword);
    }

    @Test
    public void shouldNotifyUnknownUser() {
        SystemConsole mockedSystemConsole = mock(SystemConsole.class);
        AccessView accessView = new AccessView(mockedSystemConsole);

        accessView.notifyUnknownUser();

        verify(mockedSystemConsole).display("You have to login first...");
    }

    @Test
    public void shouldNotifySuccessfulLogin() {
        SystemConsole mockedSystemConsole = mock(SystemConsole.class);
        AccessView accessView = new AccessView(mockedSystemConsole);

        accessView.notifySuccessfulLogin();

        verify(mockedSystemConsole).display("You have successfully logged in...");
    }

    @Test
    public void shouldNotifyInvalidCredential() {
        SystemConsole mockedSystemConsole = mock(SystemConsole.class);
        AccessView accessView = new AccessView(mockedSystemConsole);

        accessView.notifyInvalidCredential();

        verify(mockedSystemConsole).display("You entered invalid credential...");
    }

    @Test
    public void shouldListUserInformation() {
        SystemConsole mockedSystemConsole = mock(SystemConsole.class);
        AccessView accessView = new AccessView(mockedSystemConsole);

        accessView.listUserInformation("Varun Saraogi");

        verify(mockedSystemConsole).display("Varun Saraogi");
    }
}