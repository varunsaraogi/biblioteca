package viewTest;

import console.SystemConsole;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import view.ApplicationView;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.mockito.Mockito.verify;

public class ApplicationViewTest {
    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();

    @Before
    public void setUpStreams() {
        System.setOut(new PrintStream(outContent));
    }

    @After
    public void cleanUpStreams() {
        System.setOut(null);
    }

    @Test
    public void shouldWelcomeUser() {
        SystemConsole mockedSystemConsole = Mockito.mock(SystemConsole.class);
        ApplicationView applicationView = new ApplicationView(mockedSystemConsole);

        applicationView.welcomeUser();

        verify(mockedSystemConsole).display("Welcome to Biblioteca");
    }

    @Test
    public void shouldNotifyApplicationTermination() {
        SystemConsole mockedSystemConsole = Mockito.mock(SystemConsole.class);
        ApplicationView applicationView = new ApplicationView(mockedSystemConsole);

        applicationView.notifyTermination();

        verify(mockedSystemConsole).display("Exiting Biblioteca...");
    }
}
