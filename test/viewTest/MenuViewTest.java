package viewTest;

import console.SystemConsole;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InOrder;
import view.MenuView;

import java.util.ArrayList;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

public class MenuViewTest {
    private SystemConsole mockedSystemConsole;
    private MenuView menuView;

    @Before
    public void setup() {
        mockedSystemConsole = mock(SystemConsole.class);
        menuView = new MenuView(mockedSystemConsole);
    }

    @Test
    public void shouldFetchOption() {
        when(mockedSystemConsole.get()).thenReturn("1");

        String userChoice = menuView.fetchOption();

        verify(mockedSystemConsole).get();
        assertEquals("1", userChoice);
    }

    @Test
    public void shouldNotifyToEnterOption() {
        when(mockedSystemConsole.get()).thenReturn("1");

        menuView.fetchOption();

        verify(mockedSystemConsole).displayToGetChoice("\nEnter option : ");
    }

    @Test
    public void shouldNotifyAndFetchOptionInOrder() {
        when(mockedSystemConsole.get()).thenReturn("1");

        menuView.fetchOption();

        InOrder inOrder = inOrder(mockedSystemConsole);
        inOrder.verify(mockedSystemConsole).displayToGetChoice("\nEnter option : ");
        inOrder.verify(mockedSystemConsole).get();
    }

    @Test
    public void shouldNotifyInvalidOption() {

        menuView.notifyInvalidOption();

        verify(mockedSystemConsole).display("\nYou entered an invalid option...");

    }

    @Test
    public void shouldListOptions() {
        ArrayList<String> options = new ArrayList<>();
        options.add("1st option");
        options.add("2nd option");

        menuView.list(options);

        InOrder inOrder = inOrder(mockedSystemConsole);
        inOrder.verify(mockedSystemConsole).display("\n\n=========================Biblioteca Main Menu=========================");
        inOrder.verify(mockedSystemConsole).display("1st option");
        inOrder.verify(mockedSystemConsole).display("2nd option");
    }

}
