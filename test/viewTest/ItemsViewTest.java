package viewTest;

import console.SystemConsole;
import model.ItemType;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InOrder;
import org.mockito.Mockito;
import view.LibraryView;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

public class ItemsViewTest {
    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();

    @Before
    public void setUpStreams() {
        System.setOut(new PrintStream(outContent));
    }

    @After
    public void cleanUpStreams() {
        System.setOut(null);
    }

    @Test
    public void shouldListBooks() {
        SystemConsole mockedSystemConsole = Mockito.mock(SystemConsole.class);
        LibraryView libraryView = new LibraryView(mockedSystemConsole);

        doNothing().when(mockedSystemConsole).display("A Walk To Remember\tNicholas Sparks\t1999");
        doNothing().when(mockedSystemConsole).display("Five Point Someone\tChetan Bhagat\t2004");


        List<String> books = new ArrayList<>();
        books.add("A Walk To Remember\tNicholas Sparks\t1999");
        books.add("Five Point Someone\tChetan Bhagat\t2004");

        InOrder inOrder = inOrder(mockedSystemConsole);


        libraryView.listItems(books);

        inOrder.verify(mockedSystemConsole).display("A Walk To Remember\tNicholas Sparks\t1999");
        inOrder.verify(mockedSystemConsole).display("Five Point Someone\tChetan Bhagat\t2004");
    }

    @Test
    public void shouldNotifyForBookCheckout() {
        SystemConsole mockedSystemConsole = Mockito.mock(SystemConsole.class);
        LibraryView libraryView = new LibraryView(mockedSystemConsole);

        libraryView.notifyCheckoutFor(ItemType.BOOK);

        verify(mockedSystemConsole).display("Thank you! Enjoy the book.");
    }

    @Test
    public void shouldNotifyForBookReturn() {
        SystemConsole mockedSystemConsole = Mockito.mock(SystemConsole.class);
        LibraryView libraryView = new LibraryView(mockedSystemConsole);

        libraryView.notifyReturnFor(ItemType.BOOK);

        verify(mockedSystemConsole).display("Thank you for returning the book.");
    }

    @Test
    public void shouldNotifyForInvalidBookCheckout() {
        SystemConsole mockedSystemConsole = Mockito.mock(SystemConsole.class);
        LibraryView libraryView = new LibraryView(mockedSystemConsole);

        libraryView.notifyInvalidCheckoutFor(ItemType.BOOK);

        verify(mockedSystemConsole).display("That book is not available.");
    }

    @Test
    public void shouldNotifyForInvalidBookReturn() {
        SystemConsole mockedSystemConsole = Mockito.mock(SystemConsole.class);
        LibraryView libraryView = new LibraryView(mockedSystemConsole);

        libraryView.notifyInvalidReturnFor(ItemType.BOOK);

        verify(mockedSystemConsole).display("That is not a valid book to return.");
    }

    @Test
    public void shouldGetBookOption() {
        SystemConsole mockedSystemConsole = Mockito.mock(SystemConsole.class);
        LibraryView libraryView = new LibraryView(mockedSystemConsole);
        when(mockedSystemConsole.get()).thenReturn("1");

        String actualBookOption = libraryView.getOptionFor(ItemType.BOOK);

        verify(mockedSystemConsole).displayToGetChoice("Enter Book ID : ");
        assertEquals("1", actualBookOption);
    }
}
