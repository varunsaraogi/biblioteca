package modelTest;

import model.User;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class UserTest {
    @Test
    public void shouldReturnUserInformation() {
        User user = new User("Varun Saraogi", "svarun@thoughtworks.com", "9040398926");
        String expectedUserInformation = "Name : Varun Saraogi\tEmail : svarun@thoughtworks.com\tMobile : 9040398926";

        String actualUserInformation = user.getInformation();

        assertEquals(expectedUserInformation, actualUserInformation);
    }
}