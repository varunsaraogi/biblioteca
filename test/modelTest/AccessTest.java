package modelTest;

import model.Access;
import model.User;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class AccessTest {
    @Test
    public void noUserShouldBeLoggedInOnStart() {
        Access access = new Access();
        access.addUserCredential("111-1111", "111", new User("Varun Saraogi", "svarun@thoughtworks.com", "9040398926"));

        assertFalse(access.isLoggedIn());
    }

    @Test
    public void shouldVerifyValidUsername() {
        Access access = new Access();
        access.addUserCredential("111-1111", "111", new User("Varun Saraogi", "svarun@thoughtworks.com", "9040398926"));

        assertTrue(access.verifyUsername("111-1111"));
    }

    @Test
    public void shouldNotVerifyInvalidUsername() {
        Access access = new Access();
        access.addUserCredential("111-1111", "111", new User("Varun Saraogi", "svarun@thoughtworks.com", "9040398926"));

        assertFalse(access.verifyUsername("111-1112"));
    }

    @Test
    public void shouldVerifyValidCredentials() {
        Access access = new Access();
        access.addUserCredential("111-1111", "111", new User("Varun Saraogi", "svarun@thoughtworks.com", "9040398926"));

        assertTrue(access.verifyCredential("111-1111", "111"));
    }

    @Test
    public void shouldNotVerifyInvalidCredentials() {
        Access access = new Access();
        access.addUserCredential("111-1111", "111", new User("Varun Saraogi", "svarun@thoughtworks.com", "9040398926"));

        assertFalse(access.verifyCredential("111-1112", "111"));
    }

    @Test
    public void shouldLoginUserAfterVerifyingValidCredentials() {
        Access access = new Access();
        access.addUserCredential("111-1111", "111", new User("Varun Saraogi", "svarun@thoughtworks.com", "9040398926"));

        access.verifyCredential("111-1111", "111");

        assertTrue(access.isLoggedIn());
    }

    @Test
    public void shouldReturnLoggedInUserInformation() {
        Access access = new Access();
        access.addUserCredential("111-1111", "111", new User("Varun Saraogi", "svarun@thoughtworks.com", "9040398926"));
        String expectedUserInformation = "Name : Varun Saraogi\tEmail : svarun@thoughtworks.com\tMobile : 9040398926";
        access.verifyCredential("111-1111", "111");

        String actualUserInformation = access.getUserInformation();

        assertEquals(expectedUserInformation, actualUserInformation);
    }

    @Test
    public void shouldLogoutLoggedInUser() {
        Access access = new Access();
        access.addUserCredential("111-1111", "111", new User("Varun Saraogi", "svarun@thoughtworks.com", "9040398926"));
        access.verifyCredential("111-1111", "111");

        access.logoutUser();

        assertFalse(access.isLoggedIn());
    }
}