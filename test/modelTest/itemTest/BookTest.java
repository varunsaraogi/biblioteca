package modelTest.itemTest;

import model.item.Book;
import org.junit.Test;

import static junit.framework.TestCase.assertEquals;

public class BookTest {
    @Test
    public void shouldReturnBookDetails() {
        Book book = new Book("Five Point Someone", "Chetan Bhagat", 2004);
        String expectedDetails = "Five Point Someone\tChetan Bhagat\t2004";

        String actualDetails = book.getDetails();

        assertEquals(expectedDetails, actualDetails);
    }
}