package modelTest.itemTest;

import model.item.Book;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class ItemTest {

    @Test
    public void shouldNotBeCheckedOutInitially() {
        Book book = new Book("A Walk To Remember", "Nicholas Sparks", 1999);

        assertFalse(book.isCheckedOut());
    }

    @Test
    public void shouldUpdateCheckout() {
        Book book = new Book("A Walk To Remember", "Nicholas Sparks", 1999);

        book.updateCheckout();

        assertTrue(book.isCheckedOut());
    }

    @Test
    public void shouldUpdateReturnAfterCheckout() {
        Book book = new Book("A Walk To Remember", "Nicholas Sparks", 1999);
        book.updateCheckout();

        book.updateReturn();

        assertFalse(book.isCheckedOut());
    }
}
