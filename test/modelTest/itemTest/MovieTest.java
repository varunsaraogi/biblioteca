package modelTest.itemTest;

import model.item.Movie;
import org.junit.Test;

import static junit.framework.TestCase.assertEquals;

public class MovieTest {

    @Test
    public void shouldReturnRatedMovieDetails() {
        Movie movie = new Movie("Inception", 2010, "Christopher Nolan", "10");
        String expectedDetails = "Inception\t2010\tChristopher Nolan\t10";

        String actualDetails = movie.getDetails();

        assertEquals(expectedDetails, actualDetails);
    }

    @Test
    public void shouldReturnUnratedMovieDetails() {
        Movie movie = new Movie("Inception", 2010, "Christopher Nolan");
        String expectedDetails = "Inception\t2010\tChristopher Nolan\tunrated";

        String actualDetails = movie.getDetails();

        assertEquals(expectedDetails, actualDetails);
    }
}