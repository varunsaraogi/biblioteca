package modelTest.commandTest;

import controller.AccessController;
import model.Result;
import model.command.ShowUserInformationCommand;
import org.junit.Test;

import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

public class ShowUserInformationCommandTest {

    @Test
    public void shouldRequireAuthentication() {
        AccessController mockedAccessController = mock(AccessController.class);
        ShowUserInformationCommand showUserInformationCommand = new ShowUserInformationCommand(mockedAccessController);

        assertTrue(showUserInformationCommand.requiresAuthentication());
    }

    @Test
    public void shouldShowUserInformationOnExecution() {
        AccessController mockedAccessController = mock(AccessController.class);
        ShowUserInformationCommand showUserInformationCommand = new ShowUserInformationCommand(mockedAccessController);

        showUserInformationCommand.execute();

        verify(mockedAccessController).showUserInformation();
    }

    @Test
    public void shouldReturnTrueResultOnExecution() {
        AccessController mockedAccessController = mock(AccessController.class);
        ShowUserInformationCommand showUserInformationCommand = new ShowUserInformationCommand(mockedAccessController);

        Result result = showUserInformationCommand.execute();

        assertTrue(result.shouldProceed());
    }
}