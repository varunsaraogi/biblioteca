package modelTest.commandTest;

import controller.LibraryController;
import model.ItemType;
import model.Result;
import model.command.CheckoutItemCommand;
import org.junit.Test;

import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

public class CheckoutItemCommandTest {

    @Test
    public void shouldReturnTrueResultOnExecution() {
        LibraryController mockedLibraryController = mock(LibraryController.class);
        CheckoutItemCommand checkoutItemCommand = new CheckoutItemCommand(mockedLibraryController, ItemType.BOOK);

        Result result = checkoutItemCommand.execute();

        assertTrue(result.shouldProceed());
    }

    @Test
    public void shouldCheckoutBookOnExecution() {
        LibraryController mockedLibraryController = mock(LibraryController.class);
        CheckoutItemCommand checkoutItemCommand = new CheckoutItemCommand(mockedLibraryController, ItemType.BOOK);

        checkoutItemCommand.execute();

        verify(mockedLibraryController).initializeCheckoutFor(ItemType.BOOK);
    }


    @Test
    public void shouldRequireAuthentication() {
        LibraryController mockedLibraryController = mock(LibraryController.class);
        CheckoutItemCommand checkoutItemCommand = new CheckoutItemCommand(mockedLibraryController, ItemType.BOOK);

        assertTrue(checkoutItemCommand.requiresAuthentication());
    }
}
