package modelTest.commandTest;

import controller.LibraryController;
import model.ItemType;
import model.Result;
import model.command.ReturnItemCommand;
import org.junit.Test;

import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

public class ReturnItemCommandTest {

    @Test
    public void shouldReturnTrueResultOnExecution() {
        LibraryController mockedLibraryController = mock(LibraryController.class);
        ReturnItemCommand returnItemCommand = new ReturnItemCommand(mockedLibraryController, ItemType.BOOK);

        Result result = returnItemCommand.execute();

        assertTrue(result.shouldProceed());
    }

    @Test
    public void shouldReturnBookOnExecution() {
        LibraryController mockedLibraryController = mock(LibraryController.class);
        ReturnItemCommand returnItemCommand = new ReturnItemCommand(mockedLibraryController, ItemType.BOOK);

        returnItemCommand.execute();

        verify(mockedLibraryController).initializeReturnFor(ItemType.BOOK);
    }

    @Test
    public void shouldRequireAuthentication() {
        LibraryController mockedLibraryController = mock(LibraryController.class);
        ReturnItemCommand returnItemCommand = new ReturnItemCommand(mockedLibraryController, ItemType.BOOK);

        assertTrue(returnItemCommand.requiresAuthentication());
    }
}
