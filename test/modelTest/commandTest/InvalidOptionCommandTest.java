package modelTest.commandTest;

import controller.MenuController;
import model.Result;
import model.command.InvalidOptionCommand;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

public class InvalidOptionCommandTest {
    @Test
    public void shouldHandleInvalidOptionOnExecution() {
        MenuController mockedMenuContoller = mock(MenuController.class);
        InvalidOptionCommand invalidOptionCommand = new InvalidOptionCommand(mockedMenuContoller);

        invalidOptionCommand.execute();

        verify(mockedMenuContoller).handleInvalidOption();
    }

    @Test
    public void shouldReturnTrueResultOnExecution() {
        MenuController mockedMenuContoller = mock(MenuController.class);
        InvalidOptionCommand invalidOptionCommand = new InvalidOptionCommand(mockedMenuContoller);

        Result result = invalidOptionCommand.execute();

        assertTrue(result.shouldProceed());
    }


    @Test
    public void shouldNotRequireAuthentication() {
        MenuController mockedMenuContoller = mock(MenuController.class);
        InvalidOptionCommand invalidOptionCommand = new InvalidOptionCommand(mockedMenuContoller);

        assertFalse(invalidOptionCommand.requiresAuthentication());
    }
}
