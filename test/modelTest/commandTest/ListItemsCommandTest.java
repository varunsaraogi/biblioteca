package modelTest.commandTest;

import controller.LibraryController;
import model.ItemType;
import model.Result;
import model.command.ListItemsCommand;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

public class ListItemsCommandTest {
    @Test
    public void shouldListBooksOnExecution() {
        LibraryController mockedLibraryController = mock(LibraryController.class);
        ListItemsCommand listItemsCommand = new ListItemsCommand(mockedLibraryController, ItemType.BOOK);

        listItemsCommand.execute();

        verify(mockedLibraryController).listItems(ItemType.BOOK);
    }

    @Test
    public void shouldReturnTrueResultOnExecution() {
        LibraryController mockedLibraryController = mock(LibraryController.class);
        ListItemsCommand listItemsCommand = new ListItemsCommand(mockedLibraryController, ItemType.BOOK);

        Result result = listItemsCommand.execute();

        assertTrue(result.shouldProceed());
    }


    @Test
    public void shouldNotRequireAuthentication() {
        LibraryController mockedLibraryController = mock(LibraryController.class);
        ListItemsCommand listItemsCommand = new ListItemsCommand(mockedLibraryController, ItemType.BOOK);

        assertFalse(listItemsCommand.requiresAuthentication());
    }
}
