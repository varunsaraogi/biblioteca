package modelTest.commandTest;

import controller.AccessController;
import model.Result;
import model.command.LogoutCommand;
import org.junit.Test;

import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

public class LogoutCommandTest {

    @Test
    public void shouldRequireAuthentication() {
        AccessController mockedAccessController = mock(AccessController.class);
        LogoutCommand logoutCommand = new LogoutCommand(mockedAccessController);

        assertTrue(logoutCommand.requiresAuthentication());
    }

    @Test
    public void shouldInitializeLogoutOnExecution() {
        AccessController mockedAccessController = mock(AccessController.class);
        LogoutCommand logoutCommand = new LogoutCommand(mockedAccessController);

        logoutCommand.execute();

        verify(mockedAccessController).initializeLogout();
    }

    @Test
    public void shouldReturnTrueResultOnExecution() {
        AccessController mockedAccessController = mock(AccessController.class);
        LogoutCommand logoutCommand = new LogoutCommand(mockedAccessController);

        Result result = logoutCommand.execute();

        assertTrue(result.shouldProceed());
    }
}