package modelTest.commandTest;

import model.Result;
import model.command.QuitApplicationCommand;
import org.junit.Test;

import static org.junit.Assert.assertFalse;

public class QuitApplicationCommandTest {
    @Test
    public void shouldReturnFalseResultOnExecution() {
        QuitApplicationCommand quitApplicationCommand = new QuitApplicationCommand();

        Result result = quitApplicationCommand.execute();

        assertFalse(result.shouldProceed());
    }


    @Test
    public void shouldNotRequireAuthentication() {
        QuitApplicationCommand quitApplicationCommand = new QuitApplicationCommand();

        assertFalse(quitApplicationCommand.requiresAuthentication());
    }
}
