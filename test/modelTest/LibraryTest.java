package modelTest;

import model.ItemType;
import model.Items;
import model.Library;
import model.Result;
import model.item.Book;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class LibraryTest {
    @Test
    public void shouldCheckoutValidItem() {
        Items books = new Items(ItemType.BOOK);
        books.add(new Book("A Walk To Remember", "Nicholas Sparks", 1999));
        List<Items> itemsList = new ArrayList<>();
        itemsList.add(books);
        Library library = new Library(itemsList);

        Result result = library.checkoutItem(ItemType.BOOK, "1");

        assertTrue(result.shouldProceed());
    }

    @Test
    public void shouldNotCheckoutInvalidItem() {
        Items books = new Items(ItemType.BOOK);
        List<Items> itemsList = new ArrayList<>();
        itemsList.add(books);
        Library library = new Library(itemsList);

        Result result = library.checkoutItem(ItemType.BOOK, "1");

        assertFalse(result.shouldProceed());
    }

    @Test
    public void shouldNotCheckoutAlreadyCheckedOutItem() {
        Items books = new Items(ItemType.BOOK);
        books.add(new Book("A Walk To Remember", "Nicholas Sparks", 1999));
        List<Items> itemsList = new ArrayList<>();
        itemsList.add(books);
        Library library = new Library(itemsList);
        library.checkoutItem(ItemType.BOOK, "1");

        Result result = library.checkoutItem(ItemType.BOOK, "1");

        assertFalse(result.shouldProceed());
    }

    @Test
    public void shouldReturnValidCheckedOutItem() {
        Items books = new Items(ItemType.BOOK);
        books.add(new Book("A Walk To Remember", "Nicholas Sparks", 1999));
        List<Items> itemsList = new ArrayList<>();
        itemsList.add(books);
        Library library = new Library(itemsList);
        library.checkoutItem(ItemType.BOOK, "1");

        Result result = library.returnItem(ItemType.BOOK, "1");

        assertTrue(result.shouldProceed());
    }

    @Test
    public void shouldNotReturnInvalidItem() {
        Items books = new Items(ItemType.BOOK);
        List<Items> itemsList = new ArrayList<>();
        itemsList.add(books);
        Library library = new Library(itemsList);

        Result result = library.returnItem(ItemType.BOOK, "1");

        assertFalse(result.shouldProceed());
    }

    @Test
    public void shouldNotReturnAlreadyReturnedItem() {
        Items books = new Items(ItemType.BOOK);
        books.add(new Book("A Walk To Remember", "Nicholas Sparks", 1999));
        List<Items> itemsList = new ArrayList<>();
        itemsList.add(books);
        Library library = new Library(itemsList);
        library.checkoutItem(ItemType.BOOK, "1");
        library.returnItem(ItemType.BOOK, "1");

        Result result = library.returnItem(ItemType.BOOK, "1");

        assertFalse(result.shouldProceed());
    }
}