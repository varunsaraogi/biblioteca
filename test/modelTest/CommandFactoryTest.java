package modelTest;

import model.CommandFactory;
import model.command.Command;
import model.command.InvalidOptionCommand;
import model.command.ListItemsCommand;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;

public class CommandFactoryTest {

    @Test
    public void shouldReturnListBooksCommandForOptionOne() {
        ListItemsCommand mockedListItemsCommand = mock(ListItemsCommand.class);
        InvalidOptionCommand mockedInvalidOptionCommand = mock(InvalidOptionCommand.class);
        Map<String, Command> commands = new HashMap<>();
        commands.put("1", mockedListItemsCommand);
        CommandFactory commandFactory = new CommandFactory(commands, mockedInvalidOptionCommand);

        Command command = commandFactory.getCommandFor("1");

        assertEquals(command, mockedListItemsCommand);
    }

    @Test
    public void shouldReturnInvalidOptionCommandForOptionZero() {
        InvalidOptionCommand mockedInvalidOptionCommand = mock(InvalidOptionCommand.class);
        Map<String, Command> commands = new HashMap<>();
        CommandFactory commandFactory = new CommandFactory(commands, mockedInvalidOptionCommand);

        Command command = commandFactory.getCommandFor("0");

        assertEquals(command, mockedInvalidOptionCommand);
    }
}
