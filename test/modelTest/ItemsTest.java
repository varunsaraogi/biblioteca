package modelTest;

import model.ItemType;
import model.Items;
import model.item.Book;
import model.item.Movie;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class ItemsTest {

    @Test
    public void shouldReturnAllBookDetails() {
        Items books = new Items(ItemType.BOOK);
        books.add(new Book("A Walk To Remember", "Nicholas Sparks", 1999));
        books.add(new Book("Five Point Someone", "Chetan Bhagat", 2004));
        List<String> expectedInventory = new ArrayList<>();
        expectedInventory.add("1. A Walk To Remember\tNicholas Sparks\t1999");
        expectedInventory.add("2. Five Point Someone\tChetan Bhagat\t2004");

        List<String> actualInventory = books.getItems();

        assertEquals(expectedInventory, actualInventory);
    }

    @Test
    public void shouldCheckoutBook() {
        Items books = new Items(ItemType.BOOK);
        books.add(new Book("Five Point Someone", "Chetan Bhagat", 2004));
        books.add(new Book("A Walk To Remember", "Nicholas Sparks", 1999));
        List<String> expectedBookDetails = new ArrayList<>();
        expectedBookDetails.add("2. A Walk To Remember\tNicholas Sparks\t1999");

        books.checkoutItem(0);

        assertEquals(expectedBookDetails, books.getItems());
    }

    @Test
    public void shouldRemoveBookFromLibraryAfterCheckout() {
        Items books = new Items(ItemType.BOOK);
        books.add(new Book("A Walk To Remember", "Nicholas Sparks", 1999));
        books.add(new Book("Five Point Someone", "Chetan Bhagat", 2004));
        List<String> expectedDetails = new ArrayList<>();
        expectedDetails.add("1. A Walk To Remember\tNicholas Sparks\t1999");
        books.checkoutItem(1);

        List<String> actualDetails = books.getItems();

        assertEquals(expectedDetails, actualDetails);
    }

    @Test
    public void shouldValidateCheckoutForFirstBookWhenLibraryHasTwoInventoryWithNoPreviousCheckout() {
        Items books = new Items(ItemType.BOOK);
        books.add(new Book("A Walk To Remember", "Nicholas Sparks", 1999));
        books.add(new Book("Five Point Someone", "Chetan Bhagat", 2004));

        assertTrue(books.validateItemCheckout(0));
    }

    @Test
    public void shouldValidateCheckoutForFirstBookWhenLibraryHasTwoInventoryWithFirstBookAlreadyCheckedOut() {
        Items books = new Items(ItemType.BOOK);
        books.add(new Book("A Walk To Remember", "Nicholas Sparks", 1999));
        books.add(new Book("Five Point Someone", "Chetan Bhagat", 2004));
        books.checkoutItem(0);

        assertFalse(books.validateItemCheckout(0));
    }

    @Test
    public void shouldValidateCheckoutForBookWhenLibraryHasNoInventory() {
        Items books = new Items(ItemType.BOOK);

        assertFalse(books.validateItemCheckout(0));
    }

    @Test
    public void shouldValidateReturnForFirstBookWhenLibraryHasTwoInventoryWithNoPreviousCheckout() {
        Items books = new Items(ItemType.BOOK);
        books.add(new Book("A Walk To Remember", "Nicholas Sparks", 1999));
        books.add(new Book("Five Point Someone", "Chetan Bhagat", 2004));

        assertFalse(books.validateItemReturn(0));
    }

    @Test
    public void shouldValidateReturnForFirstBookWhenLibraryHasTwoInventoryWithFirstBookAlreadyCheckedOut() {
        Items books = new Items(ItemType.BOOK);
        books.add(new Book("A Walk To Remember", "Nicholas Sparks", 1999));
        books.add(new Book("Five Point Someone", "Chetan Bhagat", 2004));
        books.checkoutItem(0);

        assertTrue(books.validateItemReturn(0));
    }

    @Test
    public void shouldValidateReturnForBookWhenLibraryHasNoInventory() {
        Items books = new Items(ItemType.BOOK);

        assertFalse(books.validateItemReturn(0));
    }

    @Test
    public void shouldReturnAllMovieDetails() {
        Items movies = new Items(ItemType.MOVIE);
        movies.add(new Movie("Inception", 2010, "Christopher Nolan", "10"));
        movies.add(new Movie("Interstellar", 2014, "Christopher Nolan"));
        List<String> expectedMovies = new ArrayList<>();
        expectedMovies.add("1. Inception\t2010\tChristopher Nolan\t10");
        expectedMovies.add("2. Interstellar\t2014\tChristopher Nolan\tunrated");

        List<String> actualMovies = movies.getItems();

        assertEquals(expectedMovies, actualMovies);
    }

}
