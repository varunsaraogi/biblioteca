package controllerTest;

import controller.MenuController;
import model.Menu;
import org.junit.Before;
import org.junit.Test;
import view.MenuView;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

public class MenuControllerTest {

    private MenuView mockedMenuView;
    private Menu mockedMenu;
    private MenuController menuController;

    @Before
    public void setUp() throws Exception {
        mockedMenuView = mock(MenuView.class);
        mockedMenu = mock(Menu.class);
        menuController = new MenuController(mockedMenu, mockedMenuView);
    }

    @Test
    public void shouldDisplayAMenu() {
        List<String> options = new ArrayList<>();
        when(mockedMenu.getOptions()).thenReturn(options);

        menuController.displayMenu();

        verify(mockedMenuView).list(options);
    }

    @Test
    public void shouldAcceptUserOption() {
        when(mockedMenuView.fetchOption()).thenReturn("1");

        String actualUserChoice = menuController.acceptUserOption();

        verify(mockedMenuView).fetchOption();
        assertEquals("1", actualUserChoice);
    }


    @Test
    public void shouldHandleInvalidOption() {

        menuController.handleInvalidOption();

        verify(mockedMenuView).notifyInvalidOption();
    }
}
