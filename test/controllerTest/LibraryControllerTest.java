package controllerTest;

import controller.LibraryController;
import model.ItemType;
import model.Library;
import model.Result;
import org.junit.Test;
import view.LibraryView;

import java.util.ArrayList;

import static org.mockito.Mockito.*;

public class LibraryControllerTest {

    @Test
    public void shouldListInventoryOnUserChoice() {
        LibraryView mockedLibraryView = mock(LibraryView.class);
        Library mockedLibrary = mock(Library.class);
        LibraryController libraryController = new LibraryController(mockedLibrary, mockedLibraryView);
        when(mockedLibrary.getItemsDetails(ItemType.BOOK)).thenReturn(new ArrayList<>());

        libraryController.listItems(ItemType.BOOK);

        verify(mockedLibraryView).listItems(new ArrayList<>());
    }

    @Test
    public void shouldCheckoutBookFromLibrary() {
        LibraryView mockedLibraryView = mock(LibraryView.class);
        Library mockedLibrary = mock(Library.class);
        LibraryController libraryController = new LibraryController(mockedLibrary, mockedLibraryView);
        when(mockedLibraryView.getOptionFor(ItemType.BOOK)).thenReturn("1");
        when(mockedLibrary.checkoutItem(ItemType.BOOK, "1")).thenReturn(new Result(true));

        libraryController.initializeCheckoutFor(ItemType.BOOK);

        verify(mockedLibrary).checkoutItem(ItemType.BOOK, "1");
    }


    @Test
    public void shouldReturnBookToLibrary() {
        LibraryView mockedLibraryView = mock(LibraryView.class);
        Library mockedLibrary = mock(Library.class);
        LibraryController libraryController = new LibraryController(mockedLibrary, mockedLibraryView);
        when(mockedLibraryView.getOptionFor(ItemType.BOOK)).thenReturn("1");
        when(mockedLibrary.returnItem(ItemType.BOOK, "1")).thenReturn(new Result(true));

        libraryController.initializeReturnFor(ItemType.BOOK);

        verify(mockedLibrary).returnItem(ItemType.BOOK, "1");
    }

    @Test
    public void shouldNotifyCheckout() {
        LibraryView mockedLibraryView = mock(LibraryView.class);
        Library mockedLibrary = mock(Library.class);
        LibraryController libraryController = new LibraryController(mockedLibrary, mockedLibraryView);
        when(mockedLibraryView.getOptionFor(ItemType.BOOK)).thenReturn("1");
        when(mockedLibrary.checkoutItem(ItemType.BOOK, "1")).thenReturn(new Result(true));

        libraryController.initializeCheckoutFor(ItemType.BOOK);

        verify(mockedLibraryView).notifyCheckoutFor(ItemType.BOOK);
    }

    @Test
    public void shouldNotifyReturn() {
        LibraryView mockedLibraryView = mock(LibraryView.class);
        Library mockedLibrary = mock(Library.class);
        LibraryController libraryController = new LibraryController(mockedLibrary, mockedLibraryView);
        when(mockedLibraryView.getOptionFor(ItemType.BOOK)).thenReturn("1");
        when(mockedLibrary.returnItem(ItemType.BOOK, "1")).thenReturn(new Result(true));

        libraryController.initializeReturnFor(ItemType.BOOK);

        verify(mockedLibraryView).notifyReturnFor(ItemType.BOOK);
    }


    @Test
    public void shouldNotifyInvalidCheckoutIfCheckoutFailed() {
        LibraryView mockedLibraryView = mock(LibraryView.class);
        Library mockedLibrary = mock(Library.class);
        LibraryController libraryController = new LibraryController(mockedLibrary, mockedLibraryView);
        when(mockedLibraryView.getOptionFor(ItemType.BOOK)).thenReturn("2");
        when(mockedLibrary.checkoutItem(ItemType.BOOK, "2")).thenReturn(new Result(false));

        libraryController.initializeCheckoutFor(ItemType.BOOK);

        verify(mockedLibraryView).notifyInvalidCheckoutFor(ItemType.BOOK);
    }

    @Test
    public void shouldNotifyInvalidReturnIfReturnFailed() {
        LibraryView mockedLibraryView = mock(LibraryView.class);
        Library mockedLibrary = mock(Library.class);
        LibraryController libraryController = new LibraryController(mockedLibrary, mockedLibraryView);
        when(mockedLibraryView.getOptionFor(ItemType.BOOK)).thenReturn("2");
        when(mockedLibrary.returnItem(ItemType.BOOK, "2")).thenReturn(new Result(false));

        libraryController.initializeReturnFor(ItemType.BOOK);

        verify(mockedLibraryView).notifyInvalidReturnFor(ItemType.BOOK);
    }

    @Test
    public void shouldNotCheckoutSameBookTwice() {
        LibraryView mockedLibraryView = mock(LibraryView.class);
        Library mockedLibrary = mock(Library.class);
        LibraryController libraryController = new LibraryController(mockedLibrary, mockedLibraryView);
        when(mockedLibraryView.getOptionFor(ItemType.BOOK)).thenReturn("1").thenReturn("1");
        when(mockedLibrary.checkoutItem(ItemType.BOOK, "1")).thenReturn(new Result(true)).thenReturn(new Result(false));
        libraryController.initializeCheckoutFor(ItemType.BOOK);

        libraryController.initializeCheckoutFor(ItemType.BOOK);

        verify(mockedLibraryView, times(1)).notifyInvalidCheckoutFor(ItemType.BOOK);
    }
}