package controllerTest;

import controller.AccessController;
import controller.MenuController;
import model.Access;
import org.junit.Test;
import view.AccessView;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.*;

public class AccessControllerTest {
    @Test
    public void shouldNotifyForUnknownUser() {
        Access mockedAccess = mock(Access.class);
        AccessView mockedAccessView = mock(AccessView.class);
        MenuController mockedMenuController = mock(MenuController.class);
        AccessController accessController = new AccessController(mockedAccess, mockedAccessView, mockedMenuController);
        when(mockedAccess.isLoggedIn()).thenReturn(false);

        accessController.initializeAuthentication();

        verify(mockedAccessView).notifyUnknownUser();
    }

    @Test
    public void shouldNotNotifyForAlreadyAuthenticatedUser() {
        Access mockedAccess = mock(Access.class);
        AccessView mockedAccessView = mock(AccessView.class);
        MenuController mockedMenuController = mock(MenuController.class);
        AccessController accessController = new AccessController(mockedAccess, mockedAccessView, mockedMenuController);
        when(mockedAccess.isLoggedIn()).thenReturn(true);

        accessController.initializeAuthentication();

        verify(mockedAccessView, times(0)).notifyUnknownUser();
    }


    @Test
    public void shouldVerifyForAlreadyAuthenticatedUser() {
        Access mockedAccess = mock(Access.class);
        AccessView mockedAccessView = mock(AccessView.class);
        MenuController mockedMenuController = mock(MenuController.class);
        AccessController accessController = new AccessController(mockedAccess, mockedAccessView, mockedMenuController);
        when(mockedAccess.isLoggedIn()).thenReturn(true);

        assertTrue(accessController.initializeAuthentication());
    }

    @Test
    public void shouldAskForUsernameForUnknownUser() {
        Access mockedAccess = mock(Access.class);
        AccessView mockedAccessView = mock(AccessView.class);
        MenuController mockedMenuController = mock(MenuController.class);
        AccessController accessController = new AccessController(mockedAccess, mockedAccessView, mockedMenuController);
        when(mockedAccess.isLoggedIn()).thenReturn(false);

        accessController.initializeAuthentication();

        verify(mockedAccessView).getUsername();
    }

    @Test
    public void shouldVerifyAcceptedUsernameFromUnknownUser() {
        Access mockedAccess = mock(Access.class);
        AccessView mockedAccessView = mock(AccessView.class);
        MenuController mockedMenuController = mock(MenuController.class);
        AccessController accessController = new AccessController(mockedAccess, mockedAccessView, mockedMenuController);
        when(mockedAccess.isLoggedIn()).thenReturn(false);
        when(mockedAccessView.getUsername()).thenReturn("111-1111");

        accessController.initializeAuthentication();

        verify(mockedAccess).verifyUsername("111-1111");
    }

    @Test
    public void shouldAskForPasswordForUnknownUserWithValidUsername() {
        Access mockedAccess = mock(Access.class);
        AccessView mockedAccessView = mock(AccessView.class);
        MenuController mockedMenuController = mock(MenuController.class);
        AccessController accessController = new AccessController(mockedAccess, mockedAccessView, mockedMenuController);
        when(mockedAccess.isLoggedIn()).thenReturn(false);
        when(mockedAccessView.getUsername()).thenReturn("111-1111");
        when(mockedAccess.verifyUsername("111-1111")).thenReturn(true);

        accessController.initializeAuthentication();

        verify(mockedAccessView).getPassword();
    }

    @Test
    public void shouldVerifyAcceptedCredentialsFromUnknownUserWithValidUsername() {
        Access mockedAccess = mock(Access.class);
        AccessView mockedAccessView = mock(AccessView.class);
        MenuController mockedMenuController = mock(MenuController.class);
        AccessController accessController = new AccessController(mockedAccess, mockedAccessView, mockedMenuController);
        when(mockedAccess.isLoggedIn()).thenReturn(false);
        when(mockedAccessView.getUsername()).thenReturn("111-1111");
        when(mockedAccess.verifyUsername("111-1111")).thenReturn(true);
        when(mockedAccessView.getPassword()).thenReturn("111");

        accessController.initializeAuthentication();

        verify(mockedAccess).verifyCredential("111-1111", "111");
    }

    @Test
    public void shouldNotifySuccessfulLoginForAuthenticatedUser() {
        Access mockedAccess = mock(Access.class);
        AccessView mockedAccessView = mock(AccessView.class);
        MenuController mockedMenuController = mock(MenuController.class);
        AccessController accessController = new AccessController(mockedAccess, mockedAccessView, mockedMenuController);
        when(mockedAccess.isLoggedIn()).thenReturn(false);
        when(mockedAccessView.getUsername()).thenReturn("111-1111");
        when(mockedAccess.verifyUsername("111-1111")).thenReturn(true);
        when(mockedAccessView.getPassword()).thenReturn("111");
        when(mockedAccess.verifyCredential("111-1111", "111")).thenReturn(true);

        accessController.initializeAuthentication();

        verify(mockedAccessView).notifySuccessfulLogin();
    }

    @Test
    public void shouldNotifyMenuControllerWhenUserLogsIn() {
        Access mockedAccess = mock(Access.class);
        AccessView mockedAccessView = mock(AccessView.class);
        MenuController mockedMenuController = mock(MenuController.class);
        AccessController accessController = new AccessController(mockedAccess, mockedAccessView, mockedMenuController);
        when(mockedAccess.isLoggedIn()).thenReturn(false);
        when(mockedAccessView.getUsername()).thenReturn("111-1111");
        when(mockedAccess.verifyUsername("111-1111")).thenReturn(true);
        when(mockedAccessView.getPassword()).thenReturn("111");
        when(mockedAccess.verifyCredential("111-1111", "111")).thenReturn(true);

        accessController.initializeAuthentication();

        verify(mockedMenuController).userHasLoggedIn();
    }

    @Test
    public void shouldNotifyInvalidForUserCredentials() {
        Access mockedAccess = mock(Access.class);
        AccessView mockedAccessView = mock(AccessView.class);
        MenuController mockedMenuController = mock(MenuController.class);
        AccessController accessController = new AccessController(mockedAccess, mockedAccessView, mockedMenuController);
        when(mockedAccess.isLoggedIn()).thenReturn(false);
        when(mockedAccessView.getUsername()).thenReturn("111-1111");
        when(mockedAccess.verifyUsername("111-1111")).thenReturn(true);
        when(mockedAccessView.getPassword()).thenReturn("112");
        when(mockedAccess.verifyCredential("111-1111", "112")).thenReturn(false);

        accessController.initializeAuthentication();

        verify(mockedAccessView).notifyInvalidCredential();
    }

    @Test
    public void shouldVerifyInvalidForUserCredentials() {
        Access mockedAccess = mock(Access.class);
        AccessView mockedAccessView = mock(AccessView.class);
        MenuController mockedMenuController = mock(MenuController.class);
        AccessController accessController = new AccessController(mockedAccess, mockedAccessView, mockedMenuController);
        when(mockedAccess.isLoggedIn()).thenReturn(false);
        when(mockedAccessView.getUsername()).thenReturn("111-1111");
        when(mockedAccess.verifyUsername("111-1111")).thenReturn(true);
        when(mockedAccessView.getPassword()).thenReturn("112");
        when(mockedAccess.verifyCredential("111-1111", "112")).thenReturn(false);

        assertFalse(accessController.initializeAuthentication());
    }

    @Test
    public void shouldLogoutUser() {
        Access mockedAccess = mock(Access.class);
        AccessView mockedAccessView = mock(AccessView.class);
        MenuController mockedMenuController = mock(MenuController.class);
        AccessController accessController = new AccessController(mockedAccess, mockedAccessView, mockedMenuController);

        accessController.initializeLogout();

        verify(mockedAccess).logoutUser();
    }

    @Test
    public void shouldNotifyMenuControllerWhenUserLogsOut() {
        Access mockedAccess = mock(Access.class);
        AccessView mockedAccessView = mock(AccessView.class);
        MenuController mockedMenuController = mock(MenuController.class);
        AccessController accessController = new AccessController(mockedAccess, mockedAccessView, mockedMenuController);

        accessController.initializeLogout();

        verify(mockedMenuController).userHasLoggedOut();
    }
}
