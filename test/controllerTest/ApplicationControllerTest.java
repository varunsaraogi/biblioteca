package controllerTest;

import console.SystemConsole;
import controller.AccessController;
import controller.ApplicationController;
import controller.LibraryController;
import controller.MenuController;
import dependency.Dependencies;
import model.CommandFactory;
import model.Items;
import model.Menu;
import model.Result;
import model.command.*;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InOrder;
import view.ApplicationView;
import view.LibraryView;
import view.MenuView;

import static org.mockito.Mockito.*;

public class ApplicationControllerTest {
    Dependencies dependencies;
    Items mockedItems;
    SystemConsole mockedSystemConsole;
    LibraryView mockedLibraryView;
    MenuView mockedMenuView;
    ApplicationView mockedApplicationView;

    Menu mockedMenu;

    MenuController mockedMenuController;
    LibraryController mockedLibraryController;

    ListItemsCommand mockedListItemsCommand;
    QuitApplicationCommand mockedQuitApplicationCommand;
    InvalidOptionCommand mockedInvalidOptionCommand;
    CheckoutItemCommand mockedCheckoutItemCommand;
    ReturnItemCommand mockedReturnItemCommand;
    CommandFactory mockedCommandFactory;

    ApplicationController applicationController;
    AccessController mockedAccessController;

    @Before
    public void setup() {
        dependencies = new Dependencies();
        mockedItems = mock(Items.class);

        mockedSystemConsole = mock(SystemConsole.class);

        mockedLibraryView = mock(LibraryView.class);
        mockedMenuView = mock(MenuView.class);
        mockedApplicationView = mock(ApplicationView.class);

        mockedMenu = mock(Menu.class);

        mockedMenuController = mock(MenuController.class);
        mockedLibraryController = mock(LibraryController.class);

        mockedListItemsCommand = mock(ListItemsCommand.class);
        mockedQuitApplicationCommand = mock(QuitApplicationCommand.class);
        mockedInvalidOptionCommand = mock(InvalidOptionCommand.class);
        mockedCheckoutItemCommand = mock(CheckoutItemCommand.class);
        mockedReturnItemCommand = mock(ReturnItemCommand.class);
        mockedCommandFactory = mock(CommandFactory.class);

        mockedAccessController = mock(AccessController.class);

        dependencies.register(ApplicationView.class, mockedApplicationView);


        dependencies.register(MenuController.class, mockedMenuController);
        dependencies.register(LibraryController.class, mockedLibraryController);

        dependencies.register(CommandFactory.class, mockedCommandFactory);

        dependencies.register(AccessController.class, mockedAccessController);

        applicationController = new ApplicationController(dependencies);
    }

    @Test
    public void shouldWelcomeUserOnApplicationStart() {
        when(mockedMenuController.acceptUserOption()).thenReturn("1");
        when(mockedCommandFactory.getCommandFor("1")).thenReturn(mockedListItemsCommand);
        when(mockedListItemsCommand.execute()).thenReturn(new Result(false));

        applicationController.startApplication();

        verify(mockedApplicationView).welcomeUser();
    }

    @Test
    public void shouldDisplayMenu() {
        when(mockedMenuController.acceptUserOption()).thenReturn("1");
        when(mockedCommandFactory.getCommandFor("1")).thenReturn(mockedListItemsCommand);
        when(mockedListItemsCommand.execute()).thenReturn(new Result(false));

        applicationController.startApplication();

        verify(mockedMenuController).displayMenu();
    }

    @Test
    public void shouldListInventoryOnUserOption_1() {
        when(mockedMenuController.acceptUserOption()).thenReturn("1");
        when(mockedCommandFactory.getCommandFor("1")).thenReturn(mockedListItemsCommand);
        when(mockedListItemsCommand.execute()).thenReturn(new Result(false));

        applicationController.startApplication();

        verify(mockedListItemsCommand).execute();
    }

    @Test
    public void shouldCheckoutInventoryOnUserOption_2() {
        when(mockedMenuController.acceptUserOption()).thenReturn("2");
        when(mockedCommandFactory.getCommandFor("2")).thenReturn(mockedCheckoutItemCommand);
        when(mockedCheckoutItemCommand.execute()).thenReturn(new Result(false));

        applicationController.startApplication();

        verify(mockedCheckoutItemCommand).execute();
    }

    @Test
    public void shouldReturnInventoryOnUserOption_3() {
        when(mockedMenuController.acceptUserOption()).thenReturn("3");
        when(mockedCommandFactory.getCommandFor("3")).thenReturn(mockedReturnItemCommand);
        when(mockedReturnItemCommand.execute()).thenReturn(new Result(false));

        applicationController.startApplication();

        verify(mockedReturnItemCommand).execute();
    }

    @Test
    public void shouldNotifyTerminationOnUserOption_q() {
        when(mockedMenuController.acceptUserOption()).thenReturn("q");
        when(mockedCommandFactory.getCommandFor("q")).thenReturn(mockedQuitApplicationCommand);
        when(mockedQuitApplicationCommand.execute()).thenReturn(new Result(false));

        applicationController.startApplication();

        verify(mockedApplicationView).notifyTermination();
    }

    @Test
    public void shouldDisplayInvalidPromptOnUserOption_9() {
        when(mockedMenuController.acceptUserOption()).thenReturn("9");
        when(mockedCommandFactory.getCommandFor("9")).thenReturn(mockedInvalidOptionCommand);
        when(mockedInvalidOptionCommand.execute()).thenReturn(new Result(false));

        applicationController.startApplication();

        verify(mockedInvalidOptionCommand).execute();
    }


    @Test
    public void shouldPerformTasksInProperOrder() {
        when(mockedMenuController.acceptUserOption()).thenReturn("9");
        when(mockedCommandFactory.getCommandFor("9")).thenReturn(mockedInvalidOptionCommand);
        when(mockedInvalidOptionCommand.execute()).thenReturn(new Result(false));

        applicationController.startApplication();

        InOrder inOrder = inOrder(mockedApplicationView, mockedMenuController, mockedCommandFactory, mockedInvalidOptionCommand);

        inOrder.verify(mockedApplicationView).welcomeUser();
        inOrder.verify(mockedMenuController).displayMenu();
        inOrder.verify(mockedMenuController).acceptUserOption();
        inOrder.verify(mockedCommandFactory).getCommandFor("9");
        inOrder.verify(mockedInvalidOptionCommand).execute();
        inOrder.verify(mockedApplicationView).notifyTermination();
    }

    @Test
    public void shouldPerformTasksInProperOrderInLoopOnUserChoice_1_And_1() {
        when(mockedMenuController.acceptUserOption()).thenReturn("1").thenReturn("1");
        when(mockedCommandFactory.getCommandFor("1")).thenReturn(mockedListItemsCommand).thenReturn(mockedListItemsCommand);
        when(mockedListItemsCommand.execute()).thenReturn(new Result(true)).thenReturn(new Result(false));

        applicationController.startApplication();

        InOrder inOrder = inOrder(mockedApplicationView, mockedMenuController, mockedCommandFactory, mockedListItemsCommand);

        inOrder.verify(mockedApplicationView).welcomeUser();
        inOrder.verify(mockedMenuController).displayMenu();
        inOrder.verify(mockedMenuController).acceptUserOption();
        inOrder.verify(mockedCommandFactory).getCommandFor("1");
        inOrder.verify(mockedListItemsCommand).execute();
        inOrder.verify(mockedMenuController).displayMenu();
        inOrder.verify(mockedMenuController).acceptUserOption();
        inOrder.verify(mockedCommandFactory).getCommandFor("1");
        inOrder.verify(mockedListItemsCommand).execute();
        inOrder.verify(mockedApplicationView).notifyTermination();
    }


    @Test
    public void shouldListInventoryAgainOnUserOptions_1_And_1() {
        when(mockedMenuController.acceptUserOption()).thenReturn("1").thenReturn("1");
        when(mockedCommandFactory.getCommandFor("1")).thenReturn(mockedListItemsCommand).thenReturn(mockedListItemsCommand);
        when(mockedListItemsCommand.execute()).thenReturn(new Result(true)).thenReturn(new Result(false));

        applicationController.startApplication();

        verify(mockedListItemsCommand, times(2)).execute();
    }

    @Test
    public void shouldCheckForAuthenticationBeforeCommandExecution() {
        when(mockedMenuController.acceptUserOption()).thenReturn("1");
        when(mockedCommandFactory.getCommandFor("1")).thenReturn(mockedListItemsCommand);
        when(mockedListItemsCommand.execute()).thenReturn(new Result(false));
        when(mockedListItemsCommand.requiresAuthentication()).thenReturn(false);

        applicationController.startApplication();

        InOrder inOrder = inOrder(mockedListItemsCommand);
        inOrder.verify(mockedListItemsCommand).requiresAuthentication();
        inOrder.verify(mockedListItemsCommand).execute();
    }
}